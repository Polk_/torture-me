using UnityEngine;

public class Destruir : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Rigidbody>())
        {
            Destroy(other.gameObject);
        }
    }
}
