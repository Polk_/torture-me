using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab; // Prefab del NPC enemigo.
    public int numberOfEnemies = 5; // Cantidad total de enemigos que se generarán.
    public Vector3 spawnOffset = new Vector3(0, 0, 0); // Desplazamiento desde la posición del spawner.
    public float spawnInterval = 2f; // Tiempo en segundos entre cada spawn.

    private int spawnedEnemiesCount = 0; // Contador de enemigos generados.

    private void Start()
    {
        InvokeRepeating("SpawnEnemy", 0f, spawnInterval);
    }

    private void SpawnEnemy()
    {
        if (spawnedEnemiesCount < numberOfEnemies)
        {
            Vector3 spawnPosition = transform.position + spawnOffset;
            Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);
            spawnedEnemiesCount++;
        }
        else
        {
            CancelInvoke("SpawnEnemy");
        }
    }
}
