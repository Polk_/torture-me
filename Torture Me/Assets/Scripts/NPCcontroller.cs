using UnityEngine;
using UnityEngine.UI;

public class NPCcontroller : MonoBehaviour
{
    public enum State
    {
        Idle,
        Engage,
        Moving,
    }

    private Transform playerNPC; 
    private bool isMovingToPlayerNPC = false;

    // Curacion 
    public float healthGain = 20f;
    public float healthGainCooldown = 10f; 
    private float nextHealthGainTime = 5f;

    // Barra de vida 
    public Slider healthBar;

    // Salud 
    public float maxHealth = 100f;
    private float currentHealth;

    // Movimiento
    public float moveSpeed = 5f;

    // Combate
    public float detectionRadius = 5f;
    public LayerMask enemyLayer;
    public float attackDamage = 10f;
    public float attackRate = 1f;
    private float nextAttackTime = 0f;

    // Estado actual del NPC
    public State currentState = State.Idle;
    private Transform targetEnemy;
    public Transform someOtherTarget;

    private void Start()
    {
        currentHealth = maxHealth;

        if (healthBar)
        {
            healthBar.maxValue = maxHealth;
            healthBar.value = currentHealth;
        }

        playerNPC = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        switch (currentState)
        {
            case State.Idle:
                DetectEnemy();
                break;
            case State.Engage:
                ApproachEnemy();
                if (Time.time >= nextAttackTime)
                {
                    AttackEnemy();
                }
                break;

                if (currentState == State.Idle && !isMovingToPlayerNPC)
                {
                    MoveToPlayerNPC();
                }

        }

        if (Input.GetKeyDown(KeyCode.Space) && Time.time >= nextHealthGainTime)
        {
            GainHealth(healthGain);
            nextHealthGainTime = Time.time + healthGainCooldown; 
        }

        if (isMovingToPlayerNPC && playerNPC != null)
        {
            LookAtTarget(playerNPC.position);
        }
        else if (currentState == State.Moving && someOtherTarget != null) 
        {
            LookAtTarget(someOtherTarget.position);
        }

    }

    public void TakeDamage(float damageAmount)
    {
        currentHealth -= damageAmount;

        if (healthBar)
        {
            healthBar.value = currentHealth;
        }

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    private void LookAtTarget(Vector3 targetPosition)
    {
        Vector3 directionToLook = targetPosition - transform.position;
        directionToLook.y = 0; // Esto evita que el NPC "mire hacia arriba" o "hacia abajo" si hay alguna diferencia de altura.
        Quaternion targetRotation = Quaternion.LookRotation(directionToLook);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 5f); // 5f es un factor de velocidad de rotaci�n, puedes ajustarlo seg�n tus necesidades.
    }

    private void Die()
    {
        Destroy(gameObject);
    }

    private void DetectEnemy()
    {
        Collider[] detectedEnemies = Physics.OverlapSphere(transform.position, detectionRadius, enemyLayer);
        if (detectedEnemies.Length > 0)
        {
            targetEnemy = detectedEnemies[0].transform;
            currentState = State.Engage;
            isMovingToPlayerNPC = false; // Dejar de moverse al playerNPC si se detecta un enemigo.
        }
        else if (!isMovingToPlayerNPC)
        {
            MoveToPlayerNPC();
        }
    }

    private void MoveToPlayerNPC()
    {
        if (playerNPC != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, playerNPC.position, moveSpeed * Time.deltaTime);
            if (Vector3.Distance(transform.position, playerNPC.position) < 0.01f)
            {
                isMovingToPlayerNPC = false;
            }
            else
            {
                isMovingToPlayerNPC = true;
            }
        }
    }

    private void ApproachEnemy()
    {
        if (targetEnemy == null)
        {
            currentState = State.Idle;
            return;
        }

        transform.position = Vector3.MoveTowards(transform.position, targetEnemy.position, moveSpeed * Time.deltaTime);
    }

    private void AttackEnemy()
    {
        if (Vector3.Distance(transform.position, targetEnemy.position) <= 1f)
        {
            NPCcontroller enemyHealth = targetEnemy.GetComponent<NPCcontroller>();
            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(attackDamage);
                nextAttackTime = Time.time + 1f / attackRate;
            }
        }
    }
    public void GainHealth(float healthAmount)
    {
        currentHealth += healthAmount;

        // Aseg�rate de no exceder el maxHealth.
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        // Actualizar el UI si hay un healthBar asignado.
        if (healthBar)
        {
            healthBar.value = currentHealth;
        }
    }


}
